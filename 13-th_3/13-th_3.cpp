﻿#include <iostream>
#include <fstream>
#include <string>
#include <vector>

struct Element //элемент хеш-таблицы
{
	std::string key;
	int value;
	Element(std::string key = "", int value = 0) : key(key), value(value) {}
};

class HashSet
{
	int buff = 17, size = 17;
	std::vector <Element> table;

	int hash(std::string s) //хеш-функция
	{
		int h = 0, p = 1;
		for (char c : s) // h = c_0 + c_1 * p^1 + c_2 *p^2 + ... , p=3
		{
			h += c * p;
			p *= 3;
		}
		return h % buff;
	}
public:

	HashSet()//заполнение массива пустыми элементами
	{
		for (int i = 0; i < buff; i++)
			table.push_back(Element("", 0));
	}

	void add(std::string s) //добавление элемента в хеш-таблицу
	{
		int h = hash(s);
		while (table[h].key != "" && table[h].key != s && h < (size - 1))
			h++;
		if (table[h].key == "")
			table[h] = Element(s, 1);
		else if (table[h].key == s)
			table[h].value++;
		else
		{
			size++;
			table.push_back(Element(s, 1));
		}
	}

	std::string out() //функция вывода
	{
		std::string tmp;
		for (int i = 0; i < table.size(); i++)
			if (table[i].value)
				tmp += table[i].key + ' ' + std::to_string(table[i].value) + '\n';
		return tmp;
	}
};

int main()
{
	std::ifstream fin("text.txt"); 
	std::ofstream fout("out.txt");

	HashSet set;
	std::string s;
	while (fin >> s) set.add(s); // получение данных и добавление в хеш-таблицу
	fout << set.out(); //вывод в файл

	fin.close();
	fout.close();

	return 0;
}


