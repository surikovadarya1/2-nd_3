﻿#include <fstream>

int main() 
{
	std::ifstream in, in1, in2;
	std::ofstream out, out1, out2;
	
	int a, b, size=0, countA, countB, count;
	bool flag = true, Afull, Bfull;
	
	in.open("input.txt");
	out.open("result.txt");
	while (in >> a)
	{
		out << a << " "; //переписываем нашу последовательность в результирующий файл
		size++; //считаем кол-во наших элементов
	}
	in.close();
	out.close();
	
	for (int partSize = 1; partSize < size; partSize *= 2) // PartSize - длина серии (на каждом шаге увеличвается в 2 раза, а не +1)
	{
		in.open("result.txt"); //открываем для чтения
		out1.open("A.txt"); //открываем для записи А и В
		out2.open("B.txt");
		count = 0;
		while (in >> a) 
		{
			count++; //считаем сколько чисел мы записали в данный момент
			if (flag) out1 << a << " "; //в зависимости от флага записываем либо в А
			else out2 << a << " "; //либо в В файл
			if (count==partSize) //если мы записали уже требуемое кол-во чисел в серии- следующая серия запишется в другой файл
			{
				count = 0; 
				flag = !flag;
			}
		}		//после того как записали наши серии, удаляем resul.txt 
		in.close();
		out1.close();
		out2.close();
		remove("result.txt");
		
		in1.open("A.txt"); //теперь нам надо прочитать наши серии и сделать новые "слиянием"
		in2.open("B.txt");
		out.open("result.txt");		
		
		if (in1 >> a) Afull = true; //в переменной а и б хранятся соответсвенно первые числа из данных файлов
		else Afull = false;
		if (in2 >> b) Bfull = true; //Afull и Bfull отвечают за то, есть ли еще числа в соответвующих фалйах (постоянно обновляем их)
		else Bfull = false;		
		
		//теперь создаем новые серии слиянием
		for (int i = 0; i < size; i+= 2*partSize) //цикл проходит столько раз, чтобы обработали все кусочки серии
		{
			countA = 0; countB = 0; //храним сколь чисел из этой серии уже записали из соответсвующих файлов
			//countA < partSize - условие, что мы не закончили рассматривать числа из этой серии в А файле
			//Afull - условие, есть в А файле числа еще не обработанные
			while (countA < partSize && Afull && countB < partSize && Bfull)//если в обоих файлах эта серия не закончилась 		
				if (a < b) //то сравниваем их и если а меньше- записываем его 
				{
					out << a << " ";
					if (in1 >> a) Afull = true; //не забываем смотреть- есть еще числа в файле
					else Afull = false;
					countA++;
				}
				else //в противном случае записываем б
				{
					out << b << " ";
					if (in2 >> b) Bfull = true;
					else Bfull = false;
					countB++;
				}
			//после предыдущего вайла, в А или В файле остались числа, поэтому просто записываем их
			while (countA < partSize && Afull) //пока кокретная серия не закончиться в А файле
			{
				out << a << " ";
				if (in1 >> a) Afull = true;
				else Afull = false;
				countA++;
			}
			while (countB < partSize && Bfull) //пока кокретная серия не закончиться в В файле
			{
				out << b << " ";
				if (in2 >> b) Bfull = true;
				else Bfull = false;
				countB++;
			}
			//после этого уже записаны упорядочнные серии, и начинаем все сначала
			
		}
		
		in1.close();
		in2.close();
		out.close();
		remove("A.txt");
		remove("B.txt");		
	}	
	return 0;
}