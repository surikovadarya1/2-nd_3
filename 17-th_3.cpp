﻿#include <iostream>
#include <string>
#include <vector>

struct Node
{
	int value;
	Node* left = nullptr, * right = nullptr;

	Node(int x) { value = x; }

public:
	void addNode(int x) //функция добавления вершины
	{
		if (value < x && right != nullptr) right->addNode(x);
		if (value < x && right == nullptr) right = new Node(x);
		if (value > x && left != nullptr) left->addNode(x);
		if (value > x && left == nullptr) left = new Node(x);
	}

	Node* find(int x) //функция поиска вершины
	{
		if (value == x) return this;
		if (value < x && right != nullptr) return right->find(x);
		if (value > x && left != nullptr) return left->find(x);
		return nullptr;
	}

	void SaveValue(std::vector <int>& v)
	{
		v.push_back(value);
		if (left != nullptr) left->SaveValue(v);
		if (right != nullptr) right->SaveValue(v);
	}

	void remove(int x) //функция удаления вершины
	{
		if (value < x && right != nullptr && right->value != x) right->remove(x);
		if (value < x && right != nullptr && right->value == x) right = nullptr;
		if (value > x && left != nullptr && left->value != x) left->remove(x);
		if (value > x && left != nullptr && left->value == x) left = nullptr;
	}

};
/************************************************************************/
/************************************************************************/
class BinaryTree
{
	Node* root = nullptr;

public:

	void print()
	{
		std::string str = "";
		Ddirect(root, str);
		int i = 0;
		while (i < str.size())
		{
			if (str[i] == '(' && str[i + 1] == ',' && str[i + 2] == ')')
				str.erase(i, 3);
			i++;
		}
		std::cout << str << "\n";
	}

	void Ddirect(Node* n, std::string& s)
	{
		if (!n) return;
		s += std::to_string(n->value);
		s += '(';
		Ddirect(n->left, s);
		s += ',';
		Ddirect(n->right, s);
		s += ')';
	}

	void add(int x) //добавление узла в дерево
	{
		if (root == nullptr)
			root = new Node(x);
		else root->addNode(x);
	}

	void del(int x) //удаление узла из дерева
	{
		Node* n = root->find(x);
		if (n == nullptr)
		{
			std::cout << "Not found " << x << std::endl;
			return;
		}

		std::vector <int> v;
		if (n->left != nullptr) n->left->SaveValue(v);
		if (n->right != nullptr) n->right->SaveValue(v);

		if (n == root) root = nullptr;
		else root->remove(x);
		for (int i = 0; i < v.size(); i++)
			add(v[i]);
	}

	void find(int x) //поиск узла в дереве
	{
		if (root->find(x) == nullptr) std::cout << "Not found " << x << std::endl;
		else std::cout << "Found " << x << std::endl;
	}

};
/************************************************************************/
/************************************************************************/
int main()
{
	std::string s;
	int x;
	BinaryTree tree;
	while (1)
	{
		std::cout << "Avaible commands: add 'x', del 'x', find 'x'.\nYour command: ";
		std::cin >> s >> x;
		if (s == "add") tree.add(x);
		else if (s == "del") tree.del(x);
		else if (s == "find") tree.find(x);
		else
		{
			std::cout << "Not avaible command\n";
			break;
		}
		tree.print();
	}
	tree.print();

	return 0;
}