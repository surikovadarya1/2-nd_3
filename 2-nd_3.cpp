﻿#include <iostream>
#include <string>
#include <stack>

const std::string Signes = { "+-*/" };
const std::string Numerals = { "1234567890" };

int Priority(char a)
{
    switch (a)
    {
    case('+'):
        return 1;
    case('-'):
        return 1;
    case('*'):
        return 2;
    case('/'):
        return 2;
    default:
        return 0;
    }
}

float Calculate(float f1, float f2, char c)
{
    switch (c)
    {
    case('+'):
        return f1 + f2;
    case('-'):
        return f1 - f2;
    case('*'):
        return f1 * f2;
    case('/'):
        return f1 / f2;
    default:
        return 0;
    }
}

int main()
{
    std::string s;
    float tmp = 0;
    std::stack <float> numbers;
    std::stack <char> operators, skobki;
    std::cout << "Enter your expression: ";
    std::cin >> s;
    s = s + '=';

    for (int i = 0; i < s.size(); i++) //проверка на скобки, дублированные знаки
    {
        if (Signes.find(s[i]) != std::string::npos && Signes.find(s[i + 1]) != std::string::npos)
        {
            std::cout << "Error! Several operations in a row!";
            return 1;
        }
        else if (s[i] == ')' && skobki.top() == '(')
            skobki.pop();
        else if (s[i] == ')' || s[i] == '(')
            skobki.push(s[i]);
    }
    if (!skobki.empty())
    {
        std::cout << "Error! Incorrect brackets!";
        return 1;
    }

    for (int i = 0; i < s.size(); i++)
    {
        if (Numerals.find(s[i]) != std::string::npos)
        {
            tmp = tmp * 10 + float(s[i] - 48);
            if (Numerals.find(s[i + 1]) == std::string::npos)
            {
                numbers.push(tmp);
                tmp = 0;
            }
        }
        else if (Signes.find(s[i]) != std::string::npos)
        {
            if (operators.empty())
                operators.push(s[i]);
            else if (Priority(s[i]) > Priority(operators.top()))
                operators.push(s[i]);
            else
            {

                while (Priority(s[i]) <= Priority(operators.top()))
                {
                    float f2 = numbers.top();
                    numbers.pop();
                    float f1 = numbers.top();
                    numbers.pop();
                    if (f2 == 0 && operators.top() == '/')
                    {
                        std::cout << "Error! Dividing by zero!" << std::endl;
                        return 1;
                    }
                    numbers.push(Calculate(f1, f2, operators.top()));
                    operators.pop();
                    if (operators.empty())
                        break;
                }
                operators.push(s[i]);
            }
        }
        else if (s[i] == '(') //если встретили открывающую скобку - просто кладем в стек         
            operators.push(s[i]);
        else if (s[i] == ')') //если встретили закрывающую скобку - совершаем все операции, пока на встретим открывающую скобку
        {
            while (operators.top() != '(')
            {
                float f2 = numbers.top();
                numbers.pop();
                float f1 = numbers.top();
                numbers.pop();
                if (f2 == 0 && operators.top() == '/')
                {
                    std::cout << "Error! Dividing by zero!" << std::endl;
                    return 1;
                }
                numbers.push(Calculate(f1, f2, operators.top()));
                operators.pop();
            }
            operators.pop();
        }
        else if (s[i] == '=') //если встретили конец выражения - совершаем все оставшиеся операции
            while (!operators.empty())
            {
                float f2 = numbers.top();
                numbers.pop();
                float f1 = numbers.top();
                numbers.pop();
                if (f2 == 0 && operators.top() == '/')
                {
                    std::cout << "Error! Dividing by zero!" << std::endl;
                    return 1;
                }
                numbers.push(Calculate(f1, f2, operators.top()));
                operators.pop();
            }
        else //в противном случае - найден не математический символ
        {
            std::cout << "Error! Not available symbol!";
            return 1;
        }

    }

    std::cout << "Answer is " << numbers.top() << std::endl;

    return 0;
}