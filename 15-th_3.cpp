﻿#include <iostream>
#include <string>

const std::string Numerals = { "1234567890" };

struct Node
{
	int value; //значение узла
	Node* left = nullptr, * right = nullptr; //указатели на левый и правый узлы 

	Node(std::string& s) // основная функция добавления узла
	{
		value = getValue(s);
		s.erase(0, cut(value));
		if (s[0] == '(')
			s = unbracket(s);
		else return;
		if (isDigit(s[0]))
			left = new Node(s);
		if (s[0] == ',' && isDigit(s[1]))
		{
			s.erase(0, 1);
			right = new Node(s);
		}
	}

private:
	bool isDigit(char c) // проверка на то, является ли символ строки цифрой
	{
		return Numerals.find(c) != std::string::npos;
	}
	int cut(int n) //количество разрядов числа
	{
		int i = 0;
		if (n == 0) return 1;
		while (n > 0)
		{
			n /= 10;
			i++;
		}
		return i;
	}

	int getValue(std::string& s) // перевод значения из строкового типа в целочисленный
	{
		int i = 0, tmp = 0;
		for (int i = 0; i < s.size(); i++)
			if (isDigit(s[i]))
			{
				tmp = tmp * 10 + int(s[i] - 48);
				if (!isDigit(s[i + 1])) //если следующий символ не является цифрой, то возвращаем значение
					return tmp;
			}
	}

	std::string unbracket(std::string s) // функция раскрытия скобок
	{
		int brackets = 0;
		for (int i = 0; i < s.size(); i++)
		{
			if (s[i] == '(') brackets++;
			if (s[i] == ')') brackets--;
			//убираем первую и последнюю скобку
			if (!brackets)
			{
				s.erase(0, 1);
				s.erase(i - 1, 1);
				return s;
			}
		}
		return s;
	}
};
/************************************************************************/
/************************************************************************/
class BinaryTree // бинарное дерево
{
	Node* root = nullptr;

public:
	BinaryTree(std::string s)
	{
		root = new Node(s);
	}

	void direct() { Ddirect(root);  std::cout << "\n"; }
	void central() { Ccentral(root); std::cout << "\n"; }
	void inverse() { Iinverse(root); std::cout << "\n"; }

	void Ddirect(Node* n) //прямой обход
	{
		if (!n) return;
		std::cout << n->value << " ";
		Ddirect(n->left);
		Ddirect(n->right);
	}

	void Ccentral(Node* n) //центральный обход
	{
		if (!n) return;
		Ccentral(n->left);
		std::cout << n->value << " ";
		Ccentral(n->right);
	}

	void Iinverse(Node* n) //концевой обход
	{
		if (!n) return;
		Iinverse(n->left);
		Iinverse(n->right);
		std::cout << n->value << " ";
	}
};
/************************************************************************/
/************************************************************************/
int main() {
	std::string s = "8(3(1,6(4,7)),10(,14(13,)))";
	BinaryTree tree(s);
	tree.direct();
	tree.central();
	tree.inverse();

	return 0;
}